# Getting Started with <img src='/img/R_logo.png' height="30">

This project is meant to serve as a resource for pathologists interested in learning the R language and applying it to the practice of Pathology

## Useful links
[Cheatsheets](https://www.rstudio.com/resources/cheatsheets/)

## Install **R** and **R Studio**
You will need to install **R** (a programming language) as well as **R Studio** (An intergrated development enviroment)

[Download R](https://cloud.r-project.org/)

[Download R Studio](https://www.rstudio.com/products/rstudio/download/#download)

----

[R Studio Cloud](https://rstudio.cloud/)
- cloud-based R IDE
- NOT FOR USE WITH CLINICAL DATA